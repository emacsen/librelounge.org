;;; This file is part of librelounge.org, but it's the only piece
;;; released under GPLv3+, basically for compatibility reasons.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; To set up a hacking environment:
;;   guix environment -l guix.scm

(use-modules (guix packages)
             (guix licenses)
             (guix git-download)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages guile)
             (gnu packages texinfo)
             (gnu packages pkg-config))

(package
  (name "librelounge.org-website")
  (version "git")
  (source #f)
  (build-system gnu-build-system)
  (synopsis #f)
  (description #f)
  (license #f)
  (home-page #f)
  (inputs
   `(("guile" ,guile-2.2)))
  (propagated-inputs
   `(("haunt" ,haunt)
     ("guile-reader" ,guile-reader)
     ("guile-reader" ,guile-commonmark)
     ("guile-syntax-highlight" ,guile-syntax-highlight))))
