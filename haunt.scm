;;; librelounge.org website
;;; Copyright © 2016 Christopher Lemmer Webber <cwebber@dustycloud.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; GPLv3 is directly compatible with CC BY-SA 4.0, but for avoidance
;;; of confusion in mixing with website content the following code is
;;; also available under CC BY-SA 4.0 International, as published by
;;; Creative Commons.

(use-modules (ice-9 match)
             (haunt asset)
             (haunt html)
             (haunt site)
             (haunt page)
             (haunt post)
             (haunt utils)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt reader)
             (haunt reader skribe))


;;; Utilities
;;; ---------

(define %site-prefix (make-parameter ""))

(define (prefix-url url)
  (string-append (%site-prefix) url))


;;; Templates
;;; ---------

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(prefix-url (string-append "/static/css/" name ".css"))))))

(define (base-image image alt)
  `(img (@ (src ,(prefix-url (string-append "/static/images/" image)))
           (alt ,alt))))

(define (header-menu)
  `(("subscribe" ,(prefix-url "/#subscribe"))
    ("archive" ,(prefix-url "/archive/"))
    ("about" ,(prefix-url "/about/"))
    ("contact" ,(prefix-url "/content/"))))

(define* (base-tmpl site body
                    #:key title #;big-logo)
  `((doctype "html")
    (head
     (meta (@ (charset "utf-8")))
     (title ,(if title
                 (string-append title " -- Libre Lounge")
                 (site-title site)))
     ;; css
     ,(stylesheet "main")
     #;(link (@ (rel "stylesheet")
              (href ,(stylesheet "code.css"))))
     ;; atom feed
     (link (@ (rel "alternate")
              (title "Libre Lounge")
              (type "application/atom+xml")
              (href ,(prefix-url "/atom-feed.xml")))))
    (body
     (div (@ (class "main-wrapper"))
          (header (@ (id "site-header"))
                  ;; Site logo
                  (div (@ (class "header-logo-wrapper"))
                       (a (@ (href ,(prefix-url "/"))
                             (class "header-logo"))
                          ,(base-image "emacsy-logo-smaller-nocaps.png"
                            #;(if big-logo
                                "librelounge-logo-500px.png"
                                "librelounge-logo-300px.png")
                            "Libre Lounge: a casual podcast about user freedom")))
                  ;; Header menu
                  (div (@ #;,(if big-logo
                               '(class "navbar-menu big-navbar")
                               '(class "navbar-menu small-navbar"))
                        (class "navbar-menu navbar"))
                       "-=* "
                       ,@(map
                          (lambda (item)
                            (match item
                              ((name url)
                               `(div
                                 (a (@ (href ,url))
                                    ,name)))))
                          (header-menu))
                       " *=-"))
          (div (@ (class "site-main-content"))
               ,body))
     ;; TODO: Link to source.
     (div (@ (class "footer"))
          (a (@ (href "https://example.org/tell-chris-this-link-is-still-broken"))
             "Site contents")
          " released under "
          (a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/"))
             "Creative Commons Attribution-Sharealike 4.0 International")
          ".  Powered by "
          (a (@ (href "http://haunt.dthompson.us/"))
             "Haunt")
          "."))))

(define* (post-template post #:key post-link)
  `(div (@ (class "content-box blogpost"))
        (h1 (@ (class "title"))
            ,(if post-link
                 `(a (@ (href ,post-link))
                     ,(post-ref post 'title))
                 (post-ref post 'title)))
        (div (@ (class "post-about"))
             (span (@ (class "by-line"))
                   ,(post-ref post 'author))
             " -- " ,(date->string* (post-date post)))
        (div (@ (class "post-body"))
             ,(post-sxml post))))

(define (post-uri site post)
  (prefix-url
   (string-append "/episodes/" (site-post-slug site post) ".html")))

(define (collection-template site title posts prefix)
  ;; In our case, we ignore the prefix argument because
  ;; the filename generated and the pathname might not be the same.
  ;; So we use (prefix-url) instead.
  `((div (@ (class "episodes-header"))
         (h3 "recent episodes"))
    (div (@ (class "post-list"))
         ,@(map
            (lambda (post)
              (post-template post #:post-link (post-uri site post)))
            posts))))

(define librelounge-haunt-theme
  (theme #:name "Libre Lounge"
         #:layout
         (lambda (site title body)
           (base-tmpl
            site body
            #:title title))
         #:post-template post-template
         #:collection-template collection-template))


;;; Pages
(define (index-content site posts)
  `(div
    ;; Main intro
    (div (@ (class "content-box bigger-text")
            (style "margin-top: 20px; margin-bottom: 20px;"))
         (h1 (@ (class "title"))
             "What's this?")
         (p "Libre Lounge is a podcast where we casually discuss various "
            "topics involving user freedom, crossing free software, free culture, "
            "network and hosting freedom, and libre hardware designs. "
            "We discuss everything from policy and licensing to deep dives on "
            "technical topics... whatever seems interesting that week. "
            "At some point we might even have guests!")
         (p "Libre Lounge's usual hosts are "
            (a (@ (href "https://dustycloud.org/"))
               "Christopher Lemmer Webber")
            " and "
            (a (@ (href "https://blog.emacsen.net/"))
               "Serge Wroclawski") ".")
         #;(div (@ (class "code"))
              ,(highlights->sxml
                (highlight lex-scheme code-snippet)))
         (h1 (@ (class "title")
                (id "subscribe"))
             "Sounds fun!  How do I listen?")
         (p "Subscribe with your favorite podcatcher via:")
         (ul (li (a (@ (href ,(prefix-url "/atom-feed.xml")))
                    "Atom"))
             (li "RSS... Coming soon???"))
         (p "Don't have a favorite podcatcher?  We like "
            (a (@ (href "https://antennapod.org/"))
               "Antennapod") "."))

    ;; Video
    #;(div (@ (class "homepage-video-box"))
         (video (@ (controls "controls")
                   (preload "metadata")
                   (poster ,(prefix-url "/images/live_network_coding_8sync-video-preview.jpg")))
                (source (@ (src "https://archive.org/download/feb_2017-live_network_coding_8sync/live_network_coding_8sync.webm")
                           (type "video/webm"))
                        (@ (src "https://archive.org/download/feb_2017-live_network_coding_8sync/live_network_coding_8sync.mp4")
                           (type "video/mp4")))))
    #;(p (@ (style "text-align: center; margin-top: 0px;"))
       (i "Watch 8sync in action through live hacking a multiplayer game! "
          "Or watch the "
          (a (@ (href "https://ftp.heanet.ie/mirrors/fosdem-video/2017/K.4.601/networkfreedom.vp8.webm"))
             "FOSDEM edition")
          " of this talk!"))

    ;; News updates and etc
    (div (@ (class "content-box homepage-news-box")
            (style "margin-top: 20px; margin-bottom: 20px;"))
         (h1 (@ (class "title"))
             "Episodes and news")
         (ul (@ (class "homepage-news-items"))
             ,@(map (lambda (post)
                      `(li (a (@ (href ,(post-uri site post)))
                              ,(post-ref post 'title))
                           (div (@ (class "news-feed-item-date"))
                                ,(date->string* (post-date post)))))
                    (take-up-to 10 (posts/reverse-chronological posts)))))))

(define (index-page site posts)
  (make-page
   "index.html"
   (base-tmpl site
              (index-content site posts)
              ;; #:big-logo #t
              )
   sxml->html))


;;; Site

(site #:title "Libre Lounge"
      #:domain "librelounge.org"
      #:default-metadata
      '((author . "Christopher Lemmer Webber"))
      #:readers (list skribe-reader)
      #:builders (list (blog #:prefix "/episodes"
                             #:theme librelounge-haunt-theme)
                       index-page
                       (atom-feed #:blog-prefix "/episodes"
                                  #:file-name "atom-feed.xml")
                       (static-directory "static" "static")
                       (atom-feeds-by-tag)))
